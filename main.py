import zeep
import uuid
import time

wsdl = "OPC_XML_DA_1.wsdl"
client = zeep.Client(wsdl=wsdl)
service = client.create_service(
    "{http://opcfoundation.org/webservices/XMLDA/1.0/}Service",
    "http://opcxml.demo-this.com/XmlDaSampleServer/Service.asmx",
)

factory = client.type_factory("ns0")
"""
ns0:Browse(PropertyNames: xsd:QName[], LocaleID: xsd:string, ClientRequestHandle: xsd:string, ItemPath: xsd:string, ItemName: xsd:string, ContinuationPoint: xsd:string, MaxElementsReturned: xsd:int, BrowseFilter: ns0:browseFilter, ElementNameFilter: xsd:string, VendorFilter: xsd:string, ReturnAllProperties: xsd:boolean, ReturnPropertyValues: xsd:boolean, ReturnErrorText: xsd:boolean)
"""
print(
    service.Browse(
        ClientRequestHandle=str(uuid.uuid4()),
        ReturnPropertyValues=True,
        ReturnAllProperties=True,
        ItemName="Dynamic/Analog Types/Fools",
    )
)

print(
    service.Browse(
        ClientRequestHandle=str(uuid.uuid4()),
        ReturnPropertyValues=True,
        ItemName="Dynamic/Enumerated Types/Fellowship",
    )
)


request_options = factory.RequestOptions(
    ReturnErrorText=False,
    ClientRequestHandle=str(uuid.uuid4()),
    ReturnItemPath=False,
    ReturnItemTime=False,
    ReturnDiagnosticInfo=True,
)
item_list = factory.SubscribeRequestItemList(RequestedSamplingRate=1000)
"""
ns0:SubscribeRequestItem(ItemPath: xsd:string, ReqType: xsd:QName, ItemName: xsd:string, ClientItemHandle: xsd:string, Deadband: xsd:float, RequestedSamplingRate: xsd:int, EnableBuffering: xsd:boolean)
"""
item_list.Items.append(
    factory.SubscribeRequestItem(
        ItemPath="",
        ItemName="Static/Simple Types/Boolean",
    )
)
item_list.Items.append(
    factory.SubscribeRequestItem(
        ItemPath="",
        ItemName="Dynamic/Analog Types/Double[]",
    )
)
item_list.Items.append(
    factory.SubscribeRequestItem(
        ItemPath="",
        ItemName="Dynamic/Enumerated Types/Fellowship",
    )
)
return_values_on_reply = True
subscription_ping_rate = 2000
print(f"request_options={request_options}, item_list={item_list}")

response = service.Subscribe(
    Options=request_options,
    ItemList=item_list,
    ReturnValuesOnReply=False,
    SubscriptionPingRate=subscription_ping_rate,
)
print(f"subscription response={response}")
subscription_handle = response.ServerSubHandle


"""
    ns0:SubscriptionPolledRefresh(Options: ns0:RequestOptions, ServerSubHandles: xsd:string[], HoldTime: xsd:dateTime, WaitTime: xsd:int, ReturnAllItems: xsd:boolean)"""
while True:
    request_options = factory.RequestOptions(
        ReturnErrorText=False,
        ClientRequestHandle=str(uuid.uuid4()),
        ReturnItemName=True,
        ReturnItemTime=True,
        ReturnDiagnosticInfo=True,
    )
    poll_response = service.SubscriptionPolledRefresh(
        Options=request_options,
        ServerSubHandles=subscription_handle,
        ReturnAllItems=False,
    )
    print(poll_response)
    time.sleep(subscription_ping_rate / 1000.0)
